﻿using SQLite4Unity3d;

public class Words {

    [PrimaryKey, AutoIncrement]
    public int word_id { get; set; }
    public string word_name { get; set; }
    public string stock_custom { get; set; }
    public int wordscramble_mastery { get; set; }
	public int keyboard_mastery { get; set; }
	public int counting_mastery { get; set; }
    public int memory_mastery { get; set; }
    public string word_tags { get; set; }
    public string word_sound { get; set; }
    public string word_image { get; set; }

    public override string ToString() {
		return string.Format("[Word: word_id={0}, word_name={1}, stock_custom={2}, wordscramble_mastery={3}, keyboard_mastery={4}, counting_mastery={5}, memory_mastery={6}, word_tags={ 7}, word_sound={8}, word_image={9}]", word_id, word_name, stock_custom, wordscramble_mastery, keyboard_mastery, counting_mastery, memory_mastery, word_tags, word_sound, word_image);
    }
}
