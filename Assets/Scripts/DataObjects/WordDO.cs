﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordDO {

    private int idNum, wordscrambleMastery, keyboardMastery, countingMastery, memoryMastery;
    private string wordName, stockCustom, wordSound, wordImage, wordTags;

    public WordDO(int id_num, string word_name, string stock_custom, int wordscramble_mastery, int keyboard_mastery, int counting_mastery, int memory_mastery, string word_tags, string word_sound, string word_image)
    {
        idNum = id_num;
        wordName = word_name;
        stockCustom = stock_custom;
        wordscrambleMastery = wordscramble_mastery;
        keyboardMastery = keyboard_mastery;
        memoryMastery = memory_mastery;
        wordTags = word_tags;
        wordSound = word_sound;
        wordImage = word_image;
    }

    public int IdNum
    {
        get
        {
            return idNum;
        }
    }

    public string Word_name
    {
        get
        {
            return wordName;
        }
        set
        {
            wordName = value;
        }
    }

    public string StockCustom
    {
        get
        {
            return stockCustom;
        }
        set
        {
            stockCustom = value;
        }
    }

    public int Wordscramble_mastery
    {
        get
        {
            return wordscrambleMastery;
        }
        set
        {
            wordscrambleMastery = value;
        }
    }

    public int Keyboard_mastery
    {
        get
        {
            return keyboardMastery;
        }
        set
        {
            keyboardMastery = value;
        }
    }

    public int Counting_mastery
    {
        get
        {
            return countingMastery;
        }
        set
        {
            countingMastery = value;
        }
    }

    

    public int Memory_mastery
    {
        get
        {
            return memoryMastery;
        }
        set
        {
            memoryMastery = value;
        }
    }

    public string WordTags
    {
        get
        {
            return wordTags;
        }
        set
        {
            wordTags = value;
        }
    }

    public string WordSound
    {
        get
        {
            return wordSound;
        }
        set
        {
            wordSound = value;
        }
    }

    public string WordImage
    {
        get
        {
            return wordImage;
        }
        set
        {
            wordImage = value;
        }
    }



}
